var express = require('express');
var router = express.Router();
var encryptDecrypt = require('../vaultEngine/encyptDecypt.js');
var validator = require('../vaultEngine/validator.js');
const siteDb = 'userSiteInfo';
const userCredDb = 'userInfo';
const cookieAliveTime = 1800000; // 30 minutes(1000 * 60 * 30)

/* GET home page. */
router.get('/', function(req, res, next) {
  var toBeAddedUser = req.cookies.username;
  if(toBeAddedUser){
    res.render('home',{title:'SecureVault'});
  }
  res.render('welcome',{title:'SecureVault'});
});

/*GET Welcome page. */
router.get('/welcome',function(req,res,next){
  var toBeAddedUser = req.cookies.username;
  if(toBeAddedUser){
    res.render('home',{title:'SecureVault'});
  }
  res.render('welcome',{title: 'SecureVault'});
});

/*GET Home page. */
router.get('/home',function(req,res,next){
  res.render('home',{title:'SecureVault'});
});

/* Needs the request body to contain the username and password to be validated */
router.post('/login',function(req,res){
  var credentials = req.body;
  // BUG : empty string not working
  var emptyCount = validator.validateJson(credentials); //TODO - Not intuitive....false or true..
  if(emptyCount == 0){
    var encrypted = encryptDecrypt.encrypt(credentials.password); //TODO - var name should be more clear.
    var db = req.db;
    var collection = db.get(userCredDb); //TODO - collection is not intuitive variable name.
    collection.findOne({'username' : credentials.username,'password' : encrypted},function(e, doc){
      // response.setHeader('Set-Cookie', ['username=' + input.username + "'"]); // check this!!!
      
      if (doc === null){
        
        res.status(401).send('No matching records');
      } else {
        // This cookie will be alive for 30 minutes(1000 * 60 * 30)
        res.cookie('username', credentials.username, { expires: new Date(Date.now() + cookieAliveTime), httpOnly: true });
        res.send(200); 
      }
       
    });
  }else{
    res.status(400).send('Bad request - Username/Password empty!!'); //TODO - Use appropriate error codes here..
  }

});
/* POST to adduser. */
router.post('/adduser', function(req, res) {
  var db = req.db;
  var input = req.body;

  // Validate the json contents
  var nullCount = validator.validateJson(input);
  
  // TODO : May be you should validate the password format
  if(nullCount == 0){
    var encryptedPassword = encryptDecrypt.encrypt(input.password);

    var inputJson = {
      'firstname' : input.firstname ,
      'lastname' : input.lastname ,
      'username' : input.username,
      'password' : encryptedPassword,
      'email' : input.email
    }
    
    var collection = db.get(userCredDb);
    collection.insert(inputJson, function(err, result){
      if(err == null){
        res.status(200).send('Inserted successfully!');
      }else{
        res.status(501).send('Something went wrong! Request could not be completed!');
      }
    });
}else{
  res.status(400).send('Fields cannot be left blank!');
}
});

/** 
 * Method : POST
 * Required Input : Request body has to be a json - sitename,siteurl,siteusername,sitepassword
 * URL : /addSite
 * Behaviour : If no cookie(username stored was expired) was found, it responds with 404 error. 
 * */
router.post('/addSite',function(req,res){
  //TODO : check if the user is inserting a site that already exists by checking if the 
  // combination of username+sitename+siteusername is same.
  // get the username from the cookie
  var toBeAddedUser = req.cookies.username;
  if(toBeAddedUser == null){
    res.status(401).send('Login required!');
  }else{
    var siteDetails = req.body; // JSON

    // Validation to make sure no fields are empty
    var nullCount = validator.validateJson(siteDetails);
    if(nullCount == 0){

      var sitePass = encryptDecrypt.encrypt(siteDetails.sitepassword);

      // Store the encrypted password
      var toBeAdded = {
        'username' : toBeAddedUser,
        'sitename' : siteDetails.sitename,
        'siteurl' : siteDetails.siteurl,
        'siteusername' : siteDetails.siteusername,
        'sitepassword': sitePass
      }

      var db = req.db;
      var collection = db.get(siteDb);
      collection.insert(toBeAdded,function(err,result){
        if(err == null){
          res.status(200).send('Inserted successfully!');
        }else{
          res.status(501).send('Something went wrong! Request could not be completed!');
        }
      });
    }else{
      res.status(400).send('Fields cannot be left blank!');
    }
  }
});

/**
 * Method: GET
 * URL: http://localhost:3000/getAllSites
 * Pre-condition : User must be logged in.
 * Expected output: All the sites related to the current user is sent back in the response.
 */
router.get('/getAllSites',function(req,res){
  var username = req.cookies.username;
  if(username == null){
    res.status(401).send('Login required!');
  }else{
    var db = req.db;
    var sitesCollection = db.get(siteDb);
    sitesCollection.find({'username':username},{},function(err,docs){
      for(i = 0; i < docs.length; i++){
        docs[i].sitepassword = encryptDecrypt.decrypt(docs[i].sitepassword);
      }
      res.json(docs);
    });
  }
});
/**
 * Method: GET
 * Required parameter : Sitename
 * URL : http://localhost:3000/getSiteInfo?sitename=parameter
 * Pre-condition : User must be logged in.
 * Expected output : All the details of all the stored sites with the given sitname
 *                  (including decrypted password)
 *                   related to the site given in JSON.
 */
router.get('/getSiteInfo',function(req,res){

  // Gets the sitename from the parameter passed in the string
  var sitename = req.query.sitename; 
 if (sitename.includes("www")) {
      sitename = sitename.replace("www.","");
  }

  if (sitename.includes("com")) {
    sitename = sitename.replace(".com","");
  }

  var outp = sitename[0].toUpperCase()
  sitename = outp + sitename.substring(1,sitename.length);

  console.log(sitename);
  var username = req.cookies.username;
  if(username == null){
    res.status(401).send('Login required!'); // Username not found; User has to re-login.
  }else{
    if(sitename != null){
      var db = req.db;
      var collection = db.get(siteDb);
      collection.find({'username':username, 'sitename':sitename},function(e,docs){
        console.log(docs);
        if(docs == null || docs === ""){
          res.status(406).send('Site does not exist!!');
        }else{
          for(i = 0; i < docs.length; i++){
            docs[i].sitepassword = encryptDecrypt.decrypt(docs[i].sitepassword);
          }
          res.json(docs);
          
        }
      });
    }else{
      res.send('Site was null!!'); 
  }
  }
});

/**
 * Method : PUT
 * Required : JSON file related to the site for which information has to be updated.
 * URL: http://localhost:3000/editSite?sitename=sitename&siteusername=siteusername
 * Pre-condition : User must be logged in.
 * Post-condition: The edited information is updated in the database. ( the sitename cannot be edited)
 */
router.put('/editSite',function(req,res){
  var username = req.cookies.username;
  if(username == null){
    res.status(401).send('Login required!');
  }else{
    var siteDetails = req.body;
    var prevsitename = req.query.sitename;
    var prevsiteusername = req.query.siteusername;
    var nullCount = validator.validateJson(siteDetails);
    if(nullCount == 0){
      var db = req.db;
      var collection = db.get(siteDb);
      
      // get the _id
      collection.findOne({'username':username,'sitename':prevsitename,'siteusername':prevsiteusername},{'_id' : 1},function(err,doc){
        if(err){
          res.status(400).send('Something went wrong!');
        }else{
          var id = doc._id;
          console.log(id);
          
          collection.update({'_id' : id},{ $set: {'username' : username,
            'sitename' : siteDetails.sitename,
            'siteurl' : siteDetails.siteurl,
            'siteusername' : siteDetails.siteusername,
            'sitepassword': encryptDecrypt.encrypt(siteDetails.sitepassword)}},function(e,result){
              if (e) throw e;
              
              res.status(200).send('Contents successfully updated');
            });
          }
      });
      
    }else{
      res.status(400).send('Fields cannot be left blank!');
    }
  }
  

});

/**
 * Method : DELETE
 * Required : sitename and siteusername
 * URL: http://localhost:3000/deletesite?sitename=sitename&siteusername=siteusername
 * Pre-condition : User must be logged in.
 * Post-condition: Ths stored site is deleted from the DB.
 */
router.delete('/deletesite',function(req,res){

  var username = req.cookies.username;
  
  if(username == null){
    res.status(401).send('Login required!');
  }else{
    var sitename = req.query.sitename;
    var siteusername = req.query.siteusername;
    var db = req.db;
    var collection = db.get(siteDb);
    collection.findOne({'sitename':sitename,'siteusername':siteusername},function(e,doc){
      if(doc == null){
        res.status(400).send('Bad request! details not valid');
      }else{
        var id = doc._id;
        collection.remove({'_id':id, 'username':username},function(err){
          if(err == null){
            res.status(200).send('Deletion successful');
          }else{
            res.status(400).send('Deletion could not be completed');
          }
        });
      }
    });
  }
});

/**
 * Method : GET
 * URL : http://localhost:3000/logout
 * Expectation : The cookie is cleared.
 */
router.get('/logout',function(req,res){
  // clear cookie
  res.cookie('username', req.cookies.username, { expires: new Date(Date.now()-10000), httpOnly: true });
  res.status(200).send('User logged out!');
});
module.exports = router;
