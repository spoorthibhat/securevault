chrome.runtime.onInstalled.addListener(function() {
    
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [
            new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {urlContains: 'signin'}
            }),
            new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {urlContains: 'login'}
            }),
            // new chrome.declarativeContent.PageStateMatcher({
            //     pageUrl: {hostEquals: 'facebook.com'}
            // }),
            // new chrome.declarativeContent.PageStateMatcher({
            //     pageUrl: {hostEquals: 'netflix.com'}
            // }),
            // new chrome.declarativeContent.PageStateMatcher({
            //     pageUrl: {hostEquals: 'webadvisor.seattleu.edu'}
            // }),
         ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
      }]);
    });
  });