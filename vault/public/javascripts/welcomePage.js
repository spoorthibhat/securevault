$(document).ready(function(){

    $('#loginpassword').on('hover',getPasswordRules);
    $('#loginbutton').click(function(){
        authenticateCredentials(function(data){
            if(data.status == 200){
                location.replace("http://localhost:3000/home")
        }else{
            $('#login fieldset#loginField input').val('');
            alert("Re-login required: " + data.responseText);
        }
        });
    });

    $('#signupbutton').click(function(){
        signup(function(response){
            if(response.status == 200){
                $('#signup fieldset input').val('');
                alert('Sign-up complete. Please login again.');
            }else{
                alert(response.responseText);
            }
        });
    });
    
});

function getPasswordRules(){
    // do this later
}

function authenticateCredentials(callback){
    var loginCredentials = {
        'username' : $('#login fieldset#loginField input#loginusername').val(),
        'password' : $('#login fieldset#loginField input#loginpassword').val()
    };

    $.ajax({
        type: 'POST',
        data: loginCredentials,
        url: '/login',
        dataType: 'JSON',
        success: function(response){
            callback(response)
        },
        error: function(response){
            callback(response)
         }
      });
      
}

function signup(callback){

    var signupEntries = {
        'firstname' : $('#signup fieldset input#inputfirstname').val(),
        'lastname' : $('#signup fieldset input#inputlastname').val(),
        'username' : $('#signup fieldset input#inpusername').val(),
        'password' : $('#signup fieldset input#inppassword').val(),
        'email' : $('#signup fieldset input#inputemail').val()
    };

    $.ajax({
        type: 'POST',
        data: signupEntries,
        url: '/adduser',
        dataType: 'JSON',
        success: function(response){
            callback(response)
        },
        error: function(response){
            callback(response)
         }
      });

}