var siteList = [];
$(document).ready(function() {
    $("#passwordVault").click(showPasswordContent);
    $("#about").click(showAbout);
    $("#logout").click(function(){
        logOff();
    });
    
});

function showPasswordContent() {
    //document.getElementById('contents').innerHTML='';
    console.log("Clickedd");
    $("#contents").ready(function(){
        console.log("Inside Conten");
        populateSites();
    });

}
function showAbout(){
    var aboutContents = '<div class = "aboutRelated">';
    aboutContents += '<br>SecureVault is a password manager that solves the problem of having to remember 151,334,554,321 username-password combination';
    aboutContents += '<br>for 151,334,554,321 websites that we humans use day-in and day-out in our lifetime. The users only have to rembember one username and';
    aboutContents += '<br>password which then unlocks the username and password for the websites you visit through a browser plugin.';
    aboutContents += '<br>The username-password for the websites are stored in an encrypted MongoDB backed datastore.';
    aboutContents += '<br>The passwords are not stored on the browser memory.';
    aboutContents += '<br><br><b>Technologies used:</b>';
    aboutContents += '<br>Frontend: HTML, CSS, Jquery.';
    aboutContents += '<br>Backend: NodeJS, Express framework(Web application framework), Monk, Jade(Server side rendering).';
    aboutContents += '<br>Database: MongoDB</div>';

    document.getElementById('contents').innerHTML = aboutContents;
}

function showAddBtn(){
    
    var addPassword = '<br><div><button type="button" class="addPasswordBtn"'
    + 'onclick = "formToAddPassword()"> Add New Site</button><br></div><br><div class = "callout" id = "fillForm"></div>';

    $('#contents div.grid-container div#addSite-container').html(addPassword);
}

function formToAddPassword(){

    var fillForm = '';
    fillForm += '<div class = "callout-header">Add password</div>';
    fillForm += '<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span>';
    fillForm += '<div class = "callout-container">';
    fillForm += 'Site name: <input type = "text" id = "sitename" name = "sitename"><br>';
    fillForm += 'Site url: <input type = "text" id = "siteurl" name = "siteurl"><br>';
    fillForm += 'User name : <input type = "text" id = "siteusername" name = "siteusername"><br>';
    fillForm += 'Password : <input type = "text" id = "sitepassword" name = "sitepassword"><br>';
    fillForm += '<button type = "button" id = "addBtn" onclick = "putPasswordToDB()"> OK </button>';
    
    $('#contents div#addSite-container div.callout').html(fillForm);
}

function putPasswordToDB(){

    console.log('Inside password');
    var thenewPasswordDetails = {
        'sitename' : $('#contents div#addSite-container div.callout div.callout-container input#sitename').val(),
        'siteurl' : $('#contents div#addSite-container div.callout div.callout-container input#siteurl').val(),
        'siteusername' : $('#contents div#addSite-container div.callout div.callout-container input#siteusername').val(),
        'sitepassword': $('#contents div#addSite-container div.callout div.callout-container input#sitepassword').val()
    };

    $.ajax({
        url: '/addSite',
        data: thenewPasswordDetails,
        type: 'POST',
        
    }).done(function(){
        $('#contents div#addSite-container div.callout span.closebtn').trigger("click");
        populateSites();
    }).fail(function(response){
        if(response.status == 401){
            alert(response.responseText);
            location.replace("http://localhost:3000/welcome")
        }else{
            alert(response.responseText);
        }
    });
}
function populateSites(){
    var gridContents = '<div id = "passwordRelatedContents"><div class = "grid-container">';
    // gets the json through ajax call
    $.getJSON('/getAllSites',function(data){
          siteList = data;
          var i = 0;
            $.each(data,function(){
                
                gridContents += '<div class="flip-card">';
                gridContents += '<div class="flip-card-inner">';
                gridContents += '<div class="flip-card-front">';
                gridContents +=  "<h2>" + data[i].sitename + "</h2>"+ '<br> username: ' + data[i].siteusername;
                gridContents += '</div>';
                gridContents += '<div class="flip-card-back">';
                gridContents +=  '<h2><a href="'+data[i].siteurl+'">' + data[i].sitename + '</a></h2>';
                gridContents +=  '<p>Username: ' + data[i].siteusername + '</p>';
                gridContents +=  '<p>Password: ' + data[i].sitepassword + '</p>';
                gridContents += '<button type = "button" onclick = "showFormToEdit(' +i+ ')">Edit</button>';
                gridContents += '<button type = "button" class = "siteinputbtn" onclick = "deleteSite(\''+ data[i].sitename +'\',\'' + data[i].siteusername +'\')">Delete</button>';
    
                gridContents += '</div></div></div>';
                
                i++;
            });
            gridContents += '</div><div id = "addSite-container">';
            // showAddBtn();
            var addPassword = '<br><div><button type="button" class="addPasswordBtn"'
            + 'onclick = "formToAddPassword()"> Add New Site</button><br></div><br><div class = "callout" id = "fillForm"></div>';
            gridContents += addPassword;
            gridContents += '</div><div id = "callOutEdit"></div> </div>';
            //$('#contents div.grid-container').html(gridContents);
            $('#contents').html(gridContents);
            
    
    }).fail(function(response){
        alert("Error:" + response.responseText);
        location.replace("http://localhost:3000/welcome")
    });
    
}
function showFormToEdit(indexOfSiteArr){
    var thisSiteObject = siteList[indexOfSiteArr];
    
    var callOutBlock = '';
    
    callOutBlock += '<div class="callout">';
    callOutBlock += '<div class="callout-header">Edit</div>'
    callOutBlock +='<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span>'; 
    callOutBlock += '<div class="callout-container">';
    callOutBlock += 'Site name :<input type = "text" class = "siteinput" id = "sitename" value = "'+ thisSiteObject.sitename + '"><br>';
    callOutBlock += 'URL :  <input type = "text" class = "siteinput" id = "siteurl" value = "'+ thisSiteObject.siteurl + '"><br>';
    callOutBlock += 'Username :  <input type = "text" class = "siteinput" id = "siteusername" value = "'+ thisSiteObject.siteusername + '"><br>';
    callOutBlock += 'Password :  <input type = "text" class = "siteinput" id = "sitepassword" value = "'+ thisSiteObject.sitepassword + '"><br>';
    callOutBlock += '<button type = "button" class = "siteinputbtn" onclick = "editChanges(\''+ thisSiteObject.sitename + '\',\''+ thisSiteObject.siteusername+'\')">Save</button>';
    callOutBlock += '</div></div>';

    $('#contents #passwordRelatedContents #callOutEdit').html(callOutBlock);
    
}

function editChanges(prevsitename,prevsiteusername){

    var editContents = {
        'sitename' : $('#contents #passwordRelatedContents #callOutEdit .callout .callout-container input#sitename').val(),
        'siteurl' : $('#contents #passwordRelatedContents #callOutEdit .callout .callout-container input#siteurl').val(),
        'siteusername' : $('#contents #passwordRelatedContents #callOutEdit .callout .callout-container input#siteusername').val(),
        'sitepassword' : $('#contents #passwordRelatedContents #callOutEdit .callout .callout-container input#sitepassword').val()
    };

    $.ajax({
        type: 'PUT',
        data: editContents,
        url: '/editSite?sitename=' + prevsitename + '&siteusername=' + prevsiteusername,
        
    }).done(function(){
        $('.callout span.closebtn').trigger("click");
        populateSites();
    }).fail(function(response){
        if(response.status == 401){
                
            alert(response.responseText);
            location.replace("http://localhost:3000/welcome")
        }else{
            alert(response.responseText);
        }
    });
        
}

function deleteSite(sitename,siteusername){
    $.ajax({
        type : 'DELETE',
        url : '/deletesite?sitename=' + sitename + '&siteusername=' + siteusername,

    }).done(function(){
        populateSites();
    }).fail(function(response){
        if(response.status == 401){
            alert(response.responseText);
            location.replace("http://localhost:3000/welcome")
        }else{
            alert(response.responseText);
        }
    });
}

function logOff(){
    $.get("/logout").done(function(){
        location.replace("http://localhost:3000/welcome")
        alert("Logged off successfully!");
    }).fail(function(response){
        alert("Error while log out. Please try again");
    });
}